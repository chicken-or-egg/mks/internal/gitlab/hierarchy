resource "gitlab_group" "Gitlab" {
  name = "Gitlab"
  path = "gitlab"
  parent_id = gitlab_group.Internal.id
}

resource "gitlab_project" "gitlab" {
  name = "gitlab"
  path = "gitlab"
  namespace_id = gitlab_group.Gitlab.id
}

resource "gitlab_project" "hierarchy" {
  name = "hierarchy"
  path = "hierarchy"
  namespace_id = gitlab_group.Gitlab.id
}
#----------------------------------

resource "gitlab_group" "K8S-cluster" {
  name = "K8S-cluster" # Good: K8S's cluster name
  path = "k8s"
  parent_id = gitlab_group.Internal.id
}

resource "gitlab_project" "K8S-terraform" {
  name = "terraform"
  path = "terraform"
  namespace_id = gitlab_group.K8S-cluster.id
}

resource "gitlab_project" "K8S-ingress" {
  name = "ingress"
  path = "ingress"
  namespace_id = gitlab_group.K8S-cluster.id
}

resource "gitlab_project" "K8S-yaml" {
  name = "yaml"
  path = "yaml"
  namespace_id = gitlab_group.K8S-cluster.id
}