variable "gitlab_token" {}

variable "gitlab_base_url" {}

variable "insecure" {
  default = true
}