# Configure the GitLab Provider
provider "gitlab" {
  token = var.gitlab_token
  base_url = var.gitlab_base_url
  insecure = var.insecure
}


resource "gitlab_group" "Main" {
  name = "Chicken-or-egg"
  path = "main"
  description = "Main group"
}

resource "gitlab_group" "Internal" {
  name = "Internal"
  path = "internal"
  parent_id = gitlab_group.Main.id
}


resource "gitlab_group" "Products" {
  name = "Products"
  path = "product"
  parent_id = gitlab_group.Main.id
}


#------------------------------
resource "gitlab_project" "make" {
  name = "make"
  path = "make"
  namespace_id = gitlab_group.Main.id
}
